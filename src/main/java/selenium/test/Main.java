package selenium.test;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class Main {
	
	public static void main(String args[]) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		//System.setProperty("webdriver.gecko.driver", "/usr/bin/firefox");
		//WebDriver driver = new FirefoxDriver();
		
		WebElement element;
		
		driver.get("https://liftit-sandbox.com/");
		
		// enlace para ingresar
		element = driver.findElement(By.xpath("//*[@id=\"app\"]/div/ul/div[2]/li[1]/a"));
		element.click();
		
		// Login form
		element = driver.findElement(By.id("email"));
		element.click();
		element.sendKeys("qa.automation.liftit@gmail.com");
		
		element = driver.findElement(By.id("password"));
		element.click();
		element.sendKeys("L12345678");
		
		element = driver.findElement(By.id("button"));
		element.click();
		// Login form
		
		// Enlace adicionar servicio
		element = driver.findElement(By.xpath("//*[@id=\"menu-services\"]/div/a[1]"));
		element.click();
		
		// formulario de servicio
		element = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/main/div/div[2]/div[3]/div/div/div/article/div[3]/div[2]/div[2]/input"));
		element.click();
		element.sendKeys("28900");		
		
		// mover scroll hasta select HUB
		JavascriptExecutor js = (JavascriptExecutor) driver;
		element = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/main/div/div[2]/div[3]/div/div/div/article/div[3]/div[3]/div[1]"));
		js.executeScript("arguments[0].scrollIntoView();", element);
		
		// Combo select HUB
		element = driver.findElement(By.xpath("//*[@id=\"react-select-2--value\"]/div[2]/input"));
		element.sendKeys("Pruebas ");
		Thread.sleep(2000);
		element.sendKeys(Keys.ARROW_DOWN);
		element.sendKeys(Keys.ENTER);
		
		// Ruta del archivo a subir - debe estar en la raiz del proyecto,
		// o a nivel de JAR
		File archivoCSV = new File("template_optimizer.csv");
		
		// boton examinar y fileupload
		//driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Arrastra un archivo .CSV o pulsa el botón para subir un archivo.'])[1]/following::div[1]")).click();
	    //driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Examinar...'])[1]/following::input[1]")).clear();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Examinar...'])[1]/following::input[1]"))
	    	.sendKeys(archivoCSV.getAbsolutePath());
		Thread.sleep(2000);
	    
		// mover scroll hasta inicio del form
		element = driver.findElement(By.xpath("//div[contains(@class,'service-tabs')]//a[1]"));
		js.executeScript("arguments[0].scrollIntoView();", element);
		
		// continuar vehiculo y ayudante
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[2]/a[1]"));
		element.click();
		
		// Numero de ayudante
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[1]/div[2]/select[1]"));
		Select selectAyudante = new Select(element);
		selectAyudante.selectByValue("1");
		Thread.sleep(1000);
		
		// vehiculo disponible
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[3]/div[1]/div[1]/div[3]/div[1]"));
		element.click();
		Thread.sleep(1000);

		// Continuar Agendamiento
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[2]/a[2]"));
		element.click();
		
		// franja horaria
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]"));
		Actions action = new Actions(driver);
		action.moveToElement(element).click().build().perform();
		action.sendKeys(Keys.ENTER).build().perform();
		
		// scroll al calendario
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecciona el día para el que quieres agendar tu servicio'])[1]/following::div[5]"));
		js.executeScript("arguments[0].scrollIntoView();", element);
	    
		//seleccionar dia en el calendario
		element = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='do'])[1]/following::div[25]"));
		//"(.//*[normalize-space(text()) and normalize-space(.)='Tiempo'])[1]/following::p[2]"
		action.moveToElement(element).click().build().perform();
		
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]"));
		action.moveToElement(element).click().build().perform();
		element = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[4]/p[2]"));
		action.moveToElement(element).click().build().perform();
		
		element = driver.findElement(By.id("id=idsubmit"));
		action.moveToElement(element).click().build().perform();
		
	}
}
