package selenium.pages;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class ServicePage extends Page{
	
	private By declaredValue;
	private By fieldHUB; // para mover el scroll
	private By selectHUB;
	private By fileupload;
	private By serviceTab; // para mover el scroll
	private By forwStep2;
	private By forwStep3;
	private By numberAux;
	private By vehicleAvaliable;
	private By timeSelect;
	private By dayCalendar;
	private By scrollCalendar;
	private By buttonReqService;
	private By buttonSaveChanges;
	private By notification;
	private By notificationError;
	private By infoAddress;
	private By fieldAddress;

	public ServicePage(WebDriver driver) {
		super(driver);
		
		//declaredValue = By.xpath("/html/body/div[2]/div[2]/div[2]/main/div/div[2]/div[3]/div/div/div/article/div[3]/div[2]/div[2]/input");
		declaredValue = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Ingresa el valor declarado de la mercancía.'])[1]/following::input[1]");
		fieldHUB = By.xpath("/html/body/div[2]/div[2]/div[2]/main/div/div[2]/div[3]/div/div/div/article/div[3]/div[3]/div[1]");
		selectHUB = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecciona un hub.'])[1]/following::div[5]");
		fileupload = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Examinar...'])[1]/following::input[1]");
		serviceTab = By.xpath("//div[contains(@class,'service-tabs')]//a[1]");
		forwStep2 = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[2]/a[1]");
		forwStep3 = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[2]/a[2]");
		numberAux = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[1]/div[2]/select[1]");
		vehicleAvaliable = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[3]/div[1]/div[1]/div[3]/div[1]");
		timeSelect = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]");
		dayCalendar = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='do'])[1]/following::div[25]");
		scrollCalendar = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Selecciona el día para el que quieres agendar tu servicio'])[1]/following::div[5]");
		buttonReqService = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/article[1]/div[4]/p[2]");
		buttonSaveChanges = By.id("idsubmit");
		notification = By.id("notification1");
		notificationError = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::h4[1]");
		infoAddress = By.id("task_info_1");
		fieldAddress = By.id("service_tasks_0_address");
	}
	
	
	public void fillFormService(String value, String pathCSV, String newAddress) throws ElementNotInteractableException, NoSuchElementException, IllegalArgumentException{
		
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		WebElement element;
		Actions action = new Actions(getDriver());
		File fileCSV = new File(pathCSV);
		
		// 1. ingresar monto declarado
		element = getDriver().findElement(declaredValue);
		action.moveToElement(element).click().sendKeys(value).build().perform();
		// 2. mover scroll hacia el combo HUB
		element = getDriver().findElement(fieldHUB);
		js.executeScript("arguments[0].scrollIntoView();", element);
		// 3. seleccionar valor en combo HUB
		element = getDriver().findElement(selectHUB);
		action.moveToElement(element).click().sendKeys(Keys.ENTER).build().perform();;
		Util.wait(1);
		// 4. subir archivo csv
		getDriver().findElement(fileupload).sendKeys(fileCSV.getAbsolutePath());
		Util.wait(1);
		// 5. mover scroll y continuar paso 2
		element = getDriver().findElement(serviceTab);
		js.executeScript("arguments[0].scrollIntoView();", element);
		element = getDriver().findElement(forwStep2);
		action.moveToElement(element).click().build().perform();
		Util.wait(1);
		// 6. elegir ayudante
		element = getDriver().findElement(numberAux);
		Select selectAyudante = new Select(element);
		selectAyudante.selectByValue("1");
		Util.wait(1);
		// 7. elegir vehiculo
		element = getDriver().findElement(vehicleAvaliable);
		action.moveToElement(element).click().build().perform();
		Util.wait(1);
		// 8. continuar paso 3
		element = getDriver().findElement(forwStep3);
		action.moveToElement(element).click().build().perform();
		// 9. Elegir franja horaria
		element = getDriver().findElement(timeSelect);
		action.moveToElement(element).click().sendKeys(Keys.ENTER).build().perform();
		Util.wait(1);
		// 10. scroll y elegir dia en calendario
		element = getDriver().findElement(scrollCalendar);
		js.executeScript("arguments[0].scrollIntoView();", element);
		element = getDriver().findElement(dayCalendar);
		action.moveToElement(element).click().build().perform();
		Util.wait(1);
		// verificar que la notificacion de error no se muestra
		//element = getDriver().findElement(notificationError);
		//System.out.println(element.isDisplayed());
		// 11. Boton Solicitar servicio
		element = getDriver().findElement(buttonReqService);
		action.moveToElement(element).click().build().perform();
		Util.wait(1);
		// Editar la direccion de uno de los destinos
		//element = getDriver().findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/form[1]/div[2]/div[2]/div[1]/div[1]/div[2]/table[1]/thead[1]/tr[1]/th[6]"));
		//js.executeScript("arguments[0].scrollIntoView();", element);
		element = getDriver().findElement(infoAddress);
		action.moveToElement(element).click().build().perform();
		//element = getDriver().findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[2]/main[1]/form[1]/div[2]/div[2]/div[1]/div[1]/div[2]/table[1]/tbody[1]/tr[2]/td[1]/div[1]/div[1]/div[1]/div[2]/div[4]/label[1]"));
		//js.executeScript("arguments[0].scrollIntoView();", element);
		element = getDriver().findElement(fieldAddress);
		action.moveToElement(element).click().sendKeys(Keys.CLEAR).sendKeys("CRA 1 # 2 3 Bogota").build().perform();
		// 12. boton guardar cambios
		element = getDriver().findElement(buttonSaveChanges);
		action.moveToElement(element).click().build().perform();
		Util.wait(1);
		
		System.out.println(getDriver().findElement(notification).isDisplayed());
	}

}