package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginPage extends Page{

	private By userField;
	private By passwordField;
	private By buttonLogin;
	private By notificationFail;

	public LoginPage(WebDriver driver) {
		super(driver);
		userField = By.id("email");
		passwordField = By.id("password");
		buttonLogin = By.id("button");
		notificationFail = By.xpath("/html[1]/body[1]/div[3]/div[1]/div[1]");
	}

	public void login(String user, String password) {
		getDriver().findElement(userField).sendKeys(user);
		getDriver().findElement(passwordField).sendKeys(password);
		getDriver().findElement(buttonLogin).click();
	}
	
	public void assertLoginFail() {
		Assert.assertTrue(getDriver().findElement(notificationFail).isDisplayed());
	}

}
