package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class IndexPage extends Page {

	private By textUserLogin;
	private By linkCreateService;

	public IndexPage(WebDriver driver) {
		super(driver);
		// Texto superior derecha que indique que se hizo login
		textUserLogin = By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[2]/a[1]/div[1]/span[1]");
		linkCreateService = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]");
	}

	public void assertIndexLogin(String username) {
		Assert.assertTrue(getDriver().findElement(textUserLogin).getText().contains(username));
	}

	public void enterCreateService() {
		
		getDriver().findElement(linkCreateService).click();
	}	
	
}
