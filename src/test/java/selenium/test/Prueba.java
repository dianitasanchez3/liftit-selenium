package selenium.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import selenium.pages.IndexPage;
import selenium.pages.LoginPage;
import selenium.pages.ServicePage;

public class Prueba {

	WebDriver driver;

	@BeforeMethod
	public void setup() {

		driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
		driver.manage().window().maximize();
		driver.navigate().to("https://tms.liftit-sandbox.com/login");

	}

	@AfterMethod
	public void after() {
		driver.close();
	}

	@Test
	public void prueba1Login() {

		// datos login
		String username = "qa.automation.liftit@gmail.com";
		String password = "L12345678";
		// datos login

		LoginPage loginPage = new LoginPage(driver);
		loginPage.login(username, password);

		IndexPage indexPage = new IndexPage(driver);
		indexPage.assertIndexLogin(username);
	}

	@Test
	public void prueba2LoginFail() {

		// datos login
		String username = "xxqa.automation.liftit@gmail.com";
		String password = "L12345678";
		// datos login

		LoginPage loginPage = new LoginPage(driver);
		loginPage.login(username, password);

		loginPage.assertLoginFail();

	}

	@Test
	public void prueba3CreateServiceFromCSV() {

		// datos login
		String username = "qa.automation.liftit@gmail.com";
		String password = "L12345678";
		// datos login
		
		LoginPage loginPage = new LoginPage(driver);
		IndexPage indexPage = new IndexPage(driver);
		ServicePage servicePage = new ServicePage(driver);
		
		// 1.
		loginPage.login(username, password);

		// 2.
		indexPage.enterCreateService();
		
		// 3.
		servicePage.fillFormService("28900", "template_optimizer.csv", "Cra 1 # 2 3 Bogota");
	}
}
